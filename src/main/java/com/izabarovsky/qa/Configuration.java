package com.izabarovsky.qa;

// TODO: 4/30/20 Move to some property file
public class Configuration {

    private static final String BASE_URL = "https://spring-learning-app.herokuapp.com";

    public static String getBaseUrl() {
        return BASE_URL;
    }

}
