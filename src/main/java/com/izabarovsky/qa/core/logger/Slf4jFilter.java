package com.izabarovsky.qa.core.logger;

import io.restassured.filter.FilterContext;
import io.restassured.filter.OrderedFilter;
import io.restassured.filter.log.LogDetail;
import io.restassured.internal.support.Prettifier;
import io.restassured.response.Response;
import io.restassured.specification.FilterableRequestSpecification;
import io.restassured.specification.FilterableResponseSpecification;
import org.slf4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

public class Slf4jFilter implements OrderedFilter {
    private static final String NEW_LINE = System.getProperty("line.separator");
    private static Logger log;
    private ArrayList<LogDetail> logDetails;

    public Slf4jFilter(final Logger log, LogDetail... logDetails) {
        Slf4jFilter.log = log;
        this.logDetails = new ArrayList<>(Arrays.asList(logDetails));
    }

    @Override
    public int getOrder() {
        return 2000;
    }

    @Override
    public Response filter(FilterableRequestSpecification requestSpec,
                           FilterableResponseSpecification responseSpec,
                           FilterContext filterContext) {
        Prettifier prettifier = new Prettifier();
        StringBuilder builder = new StringBuilder();
        builder.append(NEW_LINE);
        if (logDetails.contains(LogDetail.METHOD)) {
            builder.append("Request method: ")
                    .append(requestSpec.getMethod())
                    .append(NEW_LINE);
        }
        if (logDetails.contains(LogDetail.URI)) {
            builder.append("Request URI:")
                    .append(NEW_LINE)
                    .append(requestSpec.getURI())
                    .append(NEW_LINE);
        }
        if (logDetails.contains(LogDetail.PARAMS)) {
            builder.append("Request params:")
                    .append(NEW_LINE)
                    .append(requestSpec.getRequestParams());
            builder.append("Query params:")
                    .append(NEW_LINE)
                    .append(requestSpec.getQueryParams());
            builder.append("Form params:")
                    .append(NEW_LINE)
                    .append(requestSpec.getFormParams());
            builder.append("Path params:")
                    .append(NEW_LINE)
                    .append(requestSpec.getNamedPathParams());
        }
        if (logDetails.contains(LogDetail.HEADERS)) {
            builder.append("Headers:")
                    .append(NEW_LINE)
                    .append(requestSpec.getHeaders().toString())
                    .append(NEW_LINE);
        }
        if (logDetails.contains(LogDetail.COOKIES)) {
            builder.append("Cookies:")
                    .append(NEW_LINE)
                    .append(requestSpec.getCookies())
                    .append(NEW_LINE);
        }
        if (logDetails.contains(LogDetail.BODY)) {
            builder.append("Request Body:");
            if (Objects.nonNull(requestSpec.getBody())) {
                builder.append(NEW_LINE)
                        .append(prettifier.getPrettifiedBodyIfPossible(requestSpec));
            } else {
                builder.append("<none>");
            }
        }
        builder.append(NEW_LINE);
        Response response = filterContext.next(requestSpec, responseSpec);
        if (logDetails.contains(LogDetail.STATUS)) {
            builder.append("Status Line: ")
                    .append(response.getStatusLine())
                    .append(NEW_LINE);
        }
        builder.append("Duration, ms: ")
                .append(response.getTimeIn(TimeUnit.MILLISECONDS))
                .append(NEW_LINE);
        if (logDetails.contains(LogDetail.HEADERS)) {
            builder.append("Headers:")
                    .append(NEW_LINE)
                    .append(response.getHeaders().toString())
                    .append(NEW_LINE);
        }
        if (logDetails.contains(LogDetail.BODY)) {
            builder.append("Response Body:");
            if (Objects.nonNull(response.getBody())) {
                builder.append(NEW_LINE)
                        .append(prettifier.getPrettifiedBodyIfPossible(response, response.getBody()));
            } else {
                builder.append("<none>");
            }
        }
        log.info(builder.toString());
        return response;
    }
}
