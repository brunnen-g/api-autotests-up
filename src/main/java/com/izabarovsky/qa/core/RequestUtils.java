package com.izabarovsky.qa.core;

import com.izabarovsky.qa.core.api.Request;
import io.restassured.response.Response;

import java.lang.reflect.Type;

public class RequestUtils {

    public static Response requestThenCheckStatus(Request request, int expectedStatus) {
        return request.execute()
                .then()
                .statusCode(expectedStatus)
                .extract()
                .response();
    }

    public static <T> T requestThenCheckStatusThenExtract(Request request, int expectedStatus, Class<T> var1) {
        return requestThenCheckStatus(request, expectedStatus)
                .then()
                .extract()
                .body()
                .as(var1);
    }

    public static <T> T requestThenCheckStatusThenExtract(Request request, int expectedStatus, Type var1) {
        return requestThenCheckStatus(request, expectedStatus)
                .then()
                .extract()
                .body()
                .as(var1);
    }

    public static long requestThenCheckStatusThenExtractLongId(Request request, int expectedStatus) {
        return requestThenCheckStatus(request, expectedStatus)
                .jsonPath()
                .getLong("id");
    }

}
