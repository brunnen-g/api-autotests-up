package com.izabarovsky.qa.core.api;

import io.restassured.RestAssured;
import io.restassured.specification.RequestSpecification;

public abstract class AbstractRequest implements Request {

    private final RequestSpecification requestSpec;

    public AbstractRequest(RequestSpecification requestSpec) {
        this.requestSpec = requestSpec;
    }

    protected RequestSpecification given() {
        return RestAssured.given().spec(requestSpec);
    }

}
