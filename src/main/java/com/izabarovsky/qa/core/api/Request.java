package com.izabarovsky.qa.core.api;

import io.restassured.response.Response;

public interface Request {
    Response execute();
}
