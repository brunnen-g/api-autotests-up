package com.izabarovsky.qa;

import com.github.javafaker.Faker;
import com.izabarovsky.qa.model.UserDto;

public class TestFaker {
    private static final Faker FAKER = new Faker();

    public static UserDto getUser() {
        UserDto user = new UserDto();
        user.setName(FAKER.dune().character());
        user.setEmail(FAKER.internet().emailAddress());
        user.setInfo(FAKER.chuckNorris().fact());
        return user;
    }
}
