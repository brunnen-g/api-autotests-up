package com.izabarovsky.qa.client;

import com.izabarovsky.qa.api.UserRequests;
import com.izabarovsky.qa.core.api.Request;
import com.izabarovsky.qa.model.UserDto;

import java.util.List;

import static com.izabarovsky.qa.core.RequestUtils.requestThenCheckStatusThenExtract;

public class DefaultUserClient implements UserClient {
    private final UserRequests userRequests = new UserRequests();

    @Override
    public List<UserDto> getUsers() {
        Request request = userRequests.newGetUsersRequest();
        UserDto[] users = requestThenCheckStatusThenExtract(request, 200, UserDto[].class);
        return List.of(users);
    }

    @Override
    public UserDto getUser(int id) {
        Request request = userRequests.newGetUserRequest().setId(id);
        return requestThenCheckStatusThenExtract(request, 200, UserDto.class);
    }

}
