package com.izabarovsky.qa.client;

import com.izabarovsky.qa.model.UserDto;

import java.util.List;

public interface UserClient {

    List<UserDto> getUsers();

    UserDto getUser(int id);

}
