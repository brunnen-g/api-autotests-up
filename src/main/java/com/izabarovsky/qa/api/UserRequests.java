package com.izabarovsky.qa.api;

import com.izabarovsky.qa.Configuration;
import com.izabarovsky.qa.api.requests.*;
import com.izabarovsky.qa.core.logger.Slf4jFilter;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.Filter;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class UserRequests {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserRequests.class);
    private static final List<Filter> FILTER_LIST = List.of(
            new Slf4jFilter(LOGGER, LogDetail.METHOD, LogDetail.URI,
                    LogDetail.BODY, LogDetail.STATUS, LogDetail.HEADERS)
    );
    private RequestSpecification requestSpecification;

    public GetUser newGetUserRequest() {
        return new GetUser(getLazyRequestSpecification());
    }

    public GetUsers newGetUsersRequest() {
        return new GetUsers(getLazyRequestSpecification());
    }

    public PostUser newPostUserRequest() {
        return new PostUser(getLazyRequestSpecification());
    }

    public PutUser newPutUserRequest() {
        return new PutUser(getLazyRequestSpecification());
    }

    public DeleteUser newDeleteUserRequest() {
        return new DeleteUser(getLazyRequestSpecification());
    }

    private RequestSpecification getNewRequestSpecification(String uri) {
        return new RequestSpecBuilder().setBaseUri(uri)
                .addFilters(FILTER_LIST)
                .setContentType(ContentType.JSON)
                .build();
    }

    private RequestSpecification getLazyRequestSpecification() {
        if (requestSpecification == null) requestSpecification = getNewRequestSpecification(Configuration.getBaseUrl());
        return requestSpecification;
    }

}
