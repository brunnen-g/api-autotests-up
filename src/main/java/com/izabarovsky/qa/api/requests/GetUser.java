package com.izabarovsky.qa.api.requests;

import com.izabarovsky.qa.api.Endpoints;
import com.izabarovsky.qa.core.api.AbstractRequest;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class GetUser extends AbstractRequest {
    private int id;

    public GetUser(RequestSpecification requestSpec) {
        super(requestSpec);
    }

    public GetUser setId(int id) {
        this.id = id;
        return this;
    }

    @Override
    public Response execute() {
        return given().pathParam("id", id)
                .get(Endpoints.USER_BY_ID);
    }
}
