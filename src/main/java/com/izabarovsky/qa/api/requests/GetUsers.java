package com.izabarovsky.qa.api.requests;

import com.izabarovsky.qa.api.Endpoints;
import com.izabarovsky.qa.core.api.AbstractRequest;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class GetUsers extends AbstractRequest {

    public GetUsers(RequestSpecification requestSpec) {
        super(requestSpec);
    }

    @Override
    public Response execute() {
        return given().get(Endpoints.USERS);
    }
}
