package com.izabarovsky.qa.api.requests;

import com.izabarovsky.qa.core.api.AbstractRequest;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class DeleteUser extends AbstractRequest {

    public DeleteUser(RequestSpecification requestSpec) {
        super(requestSpec);
    }

    @Override
    public Response execute() {
        return null;
    }
}
