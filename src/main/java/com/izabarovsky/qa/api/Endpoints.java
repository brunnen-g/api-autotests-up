package com.izabarovsky.qa.api;

public class Endpoints {
    public static final String USERS = "/users";
    public static final String USER_BY_ID = "/users/{id}";
}
