package com.izabarovsky.qa;

import com.izabarovsky.qa.model.User;
import com.izabarovsky.qa.model.UserDto;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.junit.jupiter.api.Assertions.assertFalse;

@SpringBootTest(classes = SpringConfiguration.class)
public class UserControllerTest extends BaseTest {

    @DisplayName("UsersList")
    @Test
    void getUsersList() {
        List<UserDto> users = client.getUsers();
        assertFalse(users.isEmpty(), "Users list empty");
        LOGGER.info(users.toString());
        users.forEach(s -> LOGGER.info(s.toString()));
    }

    @DisplayName("DbUsersList")
    @Test
    void getDbUsersList() {
        List<User> users = StreamSupport.stream(repository.findAll().spliterator(), false)
                .collect(Collectors.toList());
    }

}
