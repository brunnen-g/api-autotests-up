package com.izabarovsky.qa;

import com.izabarovsky.qa.client.DefaultUserClient;
import com.izabarovsky.qa.client.UserClient;
import com.izabarovsky.qa.core.extension.AllureLogsExtension;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootTest
@ExtendWith({AllureLogsExtension.class})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
//@ContextConfiguration(initializers = {BaseTest.Initializer.class})
public abstract class BaseTest {
    protected static final Logger LOGGER = LoggerFactory.getLogger(BaseTest.class);
    @Autowired
    UserRepository repository;

    protected final UserClient client = new DefaultUserClient();

    static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
        public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
            TestPropertyValues.of(
                    "spring.datasource.url=" + "jdbc:postgresql://vrovbndvwxjfgo:e59a23046c7836f0a4e33f6353dc069cf2c7beb9432ef647437e661401ddcea9@ec2-52-5-176-53.compute-1.amazonaws.com:5432/dc9pjofotq3cuk",
                    "spring.datasource.username=" + "vrovbndvwxjfgo",
                    "spring.datasource.password=" + "e59a23046c7836f0a4e33f6353dc069cf2c7beb9432ef647437e661401ddcea9"
            ).applyTo(configurableApplicationContext.getEnvironment());
        }
    }

}
